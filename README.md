# LIS 4369 - Extensible Enterprise Solutions

## Kyle Hawkins

### LIS4369 Requirements:

*Course Work Links:*

1. [A1 README.md](https://bitbucket.org/kwh10/lis4369/src/master/a1/README.md)

    * Install Python
    * Install R
    * Install R Studio
    * Install Visual Studio Code
    * Create *a1_tip_calculator* application
    * Create *a1 tip calculator Jupyter Notebook
    * Provide screenshots of installations
    * Create a Bitbucket repo
    * Complete Bitbucket Tutorial (bitbucketstationlocations)
    * Provide git command descriptions

2. [A2 README.md](https://bitbucket.org/kwh10/lis4369/src/master/a2/README.md)

    * Backward-Engineer Payroll Calculator.
    * Two modules:
        * functions.py containing:
            * get_requirements()
            * calculate_payroll()
            * print_pay()
        * main.py which imports functions.py and calls its functions.
    * Test and provide screenshots.
    * Skillsets 1, 2, and 3.

3. [A3 README.md](https://bitbucket.org/kwh10/lis4369/src/master/a3/README.md)

    1. Backward-Engineer Painting Estimator.
    2. Two modules:
        * functions.py containing:
            * get_requirements()
            * estimate_painting_cost()
            * print_painting_estimate()
            * print_painting_percentage()
        * main.py module imports the functions.py module, and calls the functions.
    3. Test and provide screenshots for IDLE and VSC.
    4. Skillsets 4, 5, and 6.

4. [A4 README.md](https://bitbucket.org/kwh10/lis4369/src/master/a4/README.md)

    1. Requirements:
        * Code and run demo.py.
        * Then use demo.py to backward-engineer the screenshots below it.
        * When displaying the required graph (see code below), answer the following question:
        * Why is the graph line split?
    2. Be sure to test your program using both IDLE and Visual Studio Code.
    4. Skillsets 10, 11, and 12.

5. [A5 README.md](https://bitbucket.org/kwh10/lis4369/src/master/a5/README.md)

    1. Requirements:
        * Complete Introduction_to_R_Setup_and_Tutorial
        * Code and run lis4369_a5.R
        * Include two plots for tutorial and two plots for a5
    2. Test with RStudio
    3. Skillsets 13, 14, and 15.

1. [P1 README.md](https://bitbucket.org/kwh10/lis4369/src/master/p1/README.md)

    1. Code and run demo.py
    2. Backward engineer provided screenshot.
    3. Test and provide screenshots for IDLE and VSC.
    4. Skillsets 7, 8, and 9.

2. [P2 README.md](https://bitbucket.org/kwh10/lis4369/src/master/p2/README.md)

    1. Referencing Assignment 5 screenshots as examples:
        * Backward-engineer the lis4369_p2_requirements.txt file
        * Include two plots
    2. Test with RStudio
