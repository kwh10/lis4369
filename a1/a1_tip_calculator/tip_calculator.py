#!/usr/bin/env python3

# Developer: Kyle Hawkins

# Course: LIS4369

# Semester: Fall 2020

#Print title and program requirements.
print("Tip Calculator\n"
    + "\nProgram Requirements:\n"
    + "1. Must use float data type for user input (except, \"Party Number\").\n"
    + "2. Must round calculations to two decimal places.\n"
    + "3. Must format currency with dollar sign, and two decimal places.")

#User input
print("\nUser input:")
subtotal = float(input("Cost of meal: "))
tax_percent = float(input("Tax percent:  "))
tip_percent = float(input("Tip percent:  "))
party_size = int(input("Party number:  "))

#Calculations
tax_actual = round(subtotal * (tax_percent / 100), 2)
due = round (subtotal + tax_actual,2)
tip_actual = round((due * (tip_percent / 100)), 2)
total = round(due + tip_actual, 2)
split = round(total / party_size, 2)

#Output
print("\nProgram Output:")
print("Subtotal:\t","${0:,.2f}".format(subtotal))
print("Tax:\t\t", "${0:,.2f}".format(tax_actual))
print("Amount Due:\t", "${0:,.2f}".format(due))
print("Gratuity:\t", "${0:,.2f}".format(tip_actual))
print("Total:\t\t", "${0:,.2f}".format(total))
print("Split " + "(" + str(party_size) + "):\t", "${0:,.2f}".format(split))
