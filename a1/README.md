> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4369 - Extensible Enterprise Solutions

## Kyle Hawkins

### Assignment 1 Requirements:

*Four Parts:*

1. Distributed Version Control with Git and Bitbucket
1. Development installations
1. Questions
1. Bitbucket repo links:
    * this assignment and
    * the completed tutorial (bitbucketstationlocations).

#### README.md file should include the following items:

* Screenshot of a1_tip_calculator application running
* Link to A1 .py file: [tip_calculator.py](a1_tip_calculator/tip_calculator.py)
* Link to A1 .ipynb file: [tip_calculator.ipynb](a1_tip_calculator/tip_calculator.ipynb)
* git commands w/short descriptions

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. git mv - Move or rename a file, a directory, or a symlink

#### Assignment Screenshots:

*Screenshot of a1_tip_calculator running (IDLE)*:

![IDLE Screenshot](img/a1_tip_calculator_idle.png)

*Screenshot of a1_tip_calculator running (Visual Studio Code)*:

![Visual Studio Code Screenshot](img/a1_tip_calculator_vs_code.png)

*A1 Jupyter Notebook*:

![Jupyter Notebook Screenshot](img/a1_jupyter_notebook.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/kwh10/bitbucketstationlocations/ "Bitbucket Station Locations")

