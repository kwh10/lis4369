"""Defines four functions:

1. get_requirements()
2. user_input()
3. calculate_payroll()
4. print_pay()

Defines three constants:
OVERTIME_MULTIPIER
HOLIDAY_MULTIPLIER
BASE_HOURS
"""

OVERTIME_MULTIPIER = 1.5
HOLIDAY_MULTIPLIER = 2.0
BASE_HOURS = 40


def get_requirements():
    """Accepts 0 args. Prints program Title and requirements."""
    print("Payroll Calculator\n"
        + "\nProgram Requirements:\n"
        + "1. Must use float data type for user input.\n"
        + "2. Overtime rate: 1.5 times hourly rate (hours over 40).\n"
        + "3. Holiday rate: 2.0 times hourly rate (all holiday hours).\n"
        + "4. Must format currency with dollar sign, and round to two "
        + "decimal places.\n"
        + "5. Create at least three functions that are called by the "
        + "program:\n"
        + "        a. main(): calls at least two other functions.\n"
        + "        b. get_requirements(): displays the program "
        + "requirements.\n"
        + "        c. calculate_payroll() : calculates an individual "
        + "one-week paycheck."
    )


def input_payroll():
    """Accepts 0 args. Gets hours and pay rate from user. 
    Returns hours_worked, holiday_hours_worked, pay_rate"""
    print("\nInput:")
    hours_worked = float(input("Enter hours worked: "))
    holiday_hours_worked = float(input("Enter holiday hours: "))
    pay_rate = float(input("Enter hourly pay rate: "))
    return hours_worked, holiday_hours_worked, pay_rate


def calculate_payroll(hours_worked, holiday_hours_worked, pay_rate):
    """Accepts 3 args. Calculates individual paycheck. 
    Returns pay_base, pay_overtime, pay_holiday, pay_gross"""
    if hours_worked <= BASE_HOURS:
        pay_base = pay_rate * hours_worked
        pay_overtime = 0
    elif hours_worked > 0:
            pay_base = pay_rate * BASE_HOURS
            pay_overtime = ((pay_rate * OVERTIME_MULTIPIER)
                * (hours_worked - BASE_HOURS))
    else:
        pay_base = 0
        pay_overtime = 0
    if hours_worked > 0:
        pay_holiday = holiday_hours_worked * (pay_rate * HOLIDAY_MULTIPLIER)
    else:
        pay_holiday = 0
    pay_gross = pay_base + pay_overtime + pay_holiday
    return pay_base, pay_overtime, pay_holiday, pay_gross
    
            
def print_pay(pay_base, pay_overtime, pay_holiday, pay_gross):
    """Accepts 4 args. Formats and prints paycheck."""
    print ("\nOutput:")
    print ("{0:<10} ${1:,.2f}".format('Base:', pay_base))
    print ("{0:<10} ${1:,.2f}".format('Overtime:', pay_overtime))
    print ("{0:<10} ${1:,.2f}".format('Holiday:', pay_holiday))
    print ("{0:<10} ${1:,.2f}".format('Gross:', pay_gross))
