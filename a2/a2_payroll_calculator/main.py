#!/usr/bin/env python3

# Developer: Kyle Hawkins

# Course: LIS4369

# Semester: Fall 2020

import functions as f

def main():
    # Print Title and requirements.
    f.get_requirements()
    # Get payroll input.
    hours_worked, holiday_hours_worked, pay_rate = f.input_payroll()
    # Pass input to calculate_payroll() and get payroll output.
    pay_base, pay_overtime, pay_holiday, pay_gross = f.calculate_payroll(
        hours_worked,holiday_hours_worked, pay_rate)
    # Print payroll output.
    f.print_pay(pay_base, pay_overtime, pay_holiday, pay_gross)


if __name__ == '__main__':
    main()