# LIS 4369 - Extensible Enterprise Solutions

## Kyle Hawkins

### Assignment 2 Requirements:

*Four Parts:*

1. Backward-Engineer Payroll Calculator.
2. Two modules:
    * functions.py containing:
        * get_requirements()
        * calculate_payroll()
        * print_pay()
    * main.py which imports functions.py and calls its functions.
3. Test and provide screenshots for IDLE and VSC.
4. Skillsets 1, 2, and 3.

#### README.md file should include the following items:

* A2 Links:
    * [main.py](a2_payroll_calculator/main.py)
    * [functions.py](a2_payroll_calculator/functions.py)
    * [payroll_calculator.ipynb](a2_payroll_calculator/payroll_calculator.ipynb)
* A2 Screenshots:
    * VSC with and without overtime
    * IDLE with and without overtime
    * Jupyter notebook

* Skillsets Links:
    * [general_functions.py](../skillsets/general_functions.py)
    * Square Feet to Acres
        * [ss1_sqft_to_acre.py](../skillsets/ss1_sqft_to_acres.py)
        * [ss1_functions.py](../skillsets/ss1_functions.py)
    * Miles Per Gallon
        * [ss2_mpg.py](../skillsets/ss2_mpg.py)
        * [ss2_functions.py](../skillsets/ss2_functions.py)
    * IT/ICT Student Percentage
        * [ss3_student_percentage.py](../skillsets/ss3_student_percentage.py)
        * [ss3_functions.py](../skillsets/ss3_functions.py)
* Skillset Screenshots:
    * Square Feet to Acres
    * Miles Per Gallon
    * IT/ICT Student Percentage

----

#### Assignment Screenshots:

*VSC Payroll No Overtime*:

![Payroll No Overtime](img/a2_payroll_calculator_vsc_no_overtime.jpg)

*VSC Payroll With Overtime*:

![Payroll With Overtime](img/a2_payroll_calculator_vsc_overtime.jpg)

*IDLE Payroll No Overtime*:

![IDLE No Overtime](img/a2_payroll_calculator_idle_no_overtime.jpg)

*IDLE Payroll With Overtime*:

![IDLE With Overtime](img/a2_payroll_calculator_idle_overtime.jpg)

*Jupyter Notebook Functions*:

![Jupyter Notebook Functions](img/a2_payroll_calculator_jupyter_notebook_functions.jpg)

*Jupyter Notebook Main*:

![Jupyter Notebook Main](img/a2_payroll_calculator_jupyter_notebook_main.jpg)

----

#### Skillset Screenshots:
*Skillset 1 - Square Feet to Acres*:

![Skillset 1 - Square Feet to Acres](../skillsets/img/ss1_sqft_to_acres.jpg)

*Skillset 2 - Miles Per Gallon*:

![Skillset 2 - Miles Per Gallon](../skillsets/img/ss2_mpg.jpg)

*Skillset 3 - IT/ICT Student Percentage*:

![Skillset 3 - IT/ICT Student Percentage](../skillsets/img/ss3_student_percentage.jpg)