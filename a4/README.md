# LIS 4369 - Extensible Enterprise Solutions

## Kyle Hawkins

### Assignment 4 Requirements:

*Three Parts:*

1. Requirements:
    * Code and run demo.py.
    * Then use demo.py to backward-engineer the screenshots below it.
    * When displaying the required graph (see code below), answer the following question:
        * Why is the graph line split?
2. Be sure to test your program using both IDLE and Visual Studio Code.
3. Skillsets 10, 11, and 12.

#### README.md file should include the following items:

* A4 Links:
    * [main.py](a4_data_analysis_2/main.py)
    * [functions.py](a4_data_analysis_2/functions.py)
    * [general_functions.py](a4_data_analysis_2/general_functions.py)
    * [a4_data_analysis_2.ipynb](a4_data_analysis_2/a4_data_analysis_2.ipynb)
* A4 Screenshots:
    * IDLE - 2 parts
    * Jupyter notebook - 3 parts
    * Figure - 1 Part

* Skillsets Links:
    * [general_functions.py](../skillsets/general_functions.py)
    * 10 - Using Dictionaries
        * [main.py](../skillsets/ss10/main.py)
        * [functions.py](../skillsets/ss10/functions.py)
    * 11 - Random Number Generator
        * [main.py](../skillsets/ss11/main.py)
        * [functions.py](../skillsets/ss11/functions.py)
    * 12 - Temperature_Conversion_Program
        * [main.py](../skillsets/ss12/main.py)
        * [functions.py](../skillsets/ss12/functions.py)
* Skillset Screenshots:
    * Using Dictionaries
    * Random Number Generator
    * Temperature_Conversion_Program

----

#### Assignment Screenshots:

*Assignment 4 IDLE-1*:

![Assignment 4 IDLE-1](img/idle_1.png)

*Assignment 4 IDLE-2*:

![Assignment 4 IDLE-2](img/idle_2.png)

*Jupyter Notebook - General Functions*:

![Jupyter Notebook Part 1](img/a4_data_analysis_2_jupyter_Page_1.png)

*Jupyter Notebook - First part of functions.py*:

![Jupyter Notebook Part 2](img/a4_data_analysis_2_jupyter_Page_2.png)

*Jupyter Notebook - main.py and first part of output*:

![Jupyter Notebook Part 3](img/a4_data_analysis_2_jupyter_Page_3.png)

*Assignment 4 Figure - Split due to NULL values*:

![Assignment 4 Figure](img/figure.png)

----

#### Skillset Screenshots:
*Skillset 10*:

![Skillset 10](../skillsets/img/ss10.png)

*Skillset 11*:

![Skillset 11](../skillsets/img/ss11.png)

*Skillset 12*:

![Skillset 12](../skillsets/img/ss12.png)