"""Assignment 4 - Data Analysis 2"""
# Developer: Kyle Hawkins

# Course: LIS4369

# Semester: Fall 2020

import functions as f
import general_functions as gf


TITLE = "Data Analysis 2"
REQUIREMENTS = """
1. Run demo.py.
2. If errors, more than likely missing installations.
3. Test Python Package Installer: pip freeze
4. Research how to install any missing packages:
5. Create at least three functions that are called by the program:
    a. main(): calls at least two other functions.
    b. get_requirements(): displays the program requirements.
    c. data_analysis_2(): displays results as per demo.py.
6. Display graph as per instructions w/in demo.py."""

def main():
    # Print Title and requirements.
    gf.get_requirements(TITLE,REQUIREMENTS,('',))
    
    # This function does the rest
    f.data_analysis_2()


if __name__ == '__main__':
    main()