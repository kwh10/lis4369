# LIS 4369 - Extensible Enterprise Solutions

## Kyle Hawkins

### Assignment 5 Requirements:

*Three Parts:*

1. Requirements:
    * Complete Introduction_to_R_Setup_and_Tutorial
    * Code and run lis4369_a5.R
    * Include two plots for tutorial and two plots for a5
2. Test with RStudio
3. Skillsets 13, 14, and 15.

#### README.md file should include the following items:

* A5 Links:
    * [learn_to_use_r.R](r_tutorial/learn_to_use_r.R)
    * [lis4369_a5.R](a5_introduction_to_r/lis4369_a5.R)
* A5 Screenshots:
    * Tutorial - 4 parts
    * A5 - 4 parts

* Skillsets Links:
    * [general_functions.py](../skillsets/general_functions.py)
    * 13 - Sphere_Volume_Calculator
        * [main.py](../skillsets/ss13/main.py)
        * [functions.py](../skillsets/ss13/functions.py)
    * 14 - Calculator_with_Error_Handling
        * [main.py](../skillsets/ss14/main.py)
        * [functions.py](../skillsets/ss14/functions.py)
    * 15 - File_Write/Read
        * [main.py](../skillsets/ss15/main.py)
        * [functions.py](../skillsets/ss15/functions.py)
* Skillset Screenshots:
    * Sphere_Volume_Calculator - 1 part
    * Calculator_with_Error_Handling - 1 part
    * File_Write/Read - 2 parts

----

#### Assignment Screenshots:

*Tutorial RStudio*:

![Assignment 5 RStudio](img/tutorial/rstudio.jpg)

*Tutorial R Console*:

![Tutorial R Console](img/tutorial/r_console.jpg)

*Tutorial Graph 1*:

![Tutorial Graph 1](img/tutorial/graphs_1.jpeg)

*Tutorial Graph 2*:

![Tutorial Graph 2](img/tutorial/graphs_2.jpeg)

*A5 Output Part 1*:

![A5 Output Part 1](img/lis4369_a5_output_1.png)

*A5 Output Part 2*:

![A5 Output Part 2](img/lis4369_a5_output_2.png)

*A5 Plot 1*:

![A5 Plot 1](img/lis4369_a5_plot_1.png)

*A5 Plot 2*:

![A5 Plot 2](img/lis4369_a5_plot_2.png)

----

#### Skillset Screenshots:
*Skillset 13*:

![Skillset 13](../skillsets/img/ss13.png)

*Skillset 14*:

![Skillset 14](../skillsets/img/ss14.png)

*Skillset 15 part 1*:

![Skillset 15 part 1](../skillsets/img/ss15-1.png)

*Skillset 15 part 2*:

![Skillset 15 part 2](../skillsets/img/ss15-2.png)