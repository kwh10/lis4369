"""
Defines functions specific to Skillset 11.
Functions:
random_input
random_randint
random_shuffle
"""
import math
### CONSTANTS ###
# None
INCH_PER_GALLON = 0.004329

### FUNCTIONS ###
def sphere_want(printed_in):
    if printed_in == False:
        print("\nInput: ")
        printed_in = True
    while True:
        try:
            choice = input("Do you want to calculate a sphere volume (y or n)? ").lower()
            if choice == 'y' or choice == 'n':
                break
            else:
                raise 'incorrect'
        except:
            print("\ny or n. Please try again.")
            pass
    return choice,printed_in


def sphere_input(printed_out):
    while True:
        try:
            if printed_out == False:
                print("\nOutput: ")
                printed_out = True
            diameter = int(input("Please enter diameter in inches: "))
            if True:
                break
        except:
            print("\nNot valid integer!")
            pass
    return diameter,printed_out

def calc_volume(diameter):
    radius = diameter/2
    volume = (4/3)*math.pi*math.pow(radius,3)
    return volume

def calc_gallons(cubic_inches):
    gallons = cubic_inches * INCH_PER_GALLON
    return gallons

### ALIASES ###
# None

