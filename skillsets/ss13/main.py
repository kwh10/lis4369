"""Skillset 13"""
# Developer: Kyle Hawkins

# Course: LIS4369

# Semester: Fall 2020


### IMPORTS ###
# Import general functions for skillsets.
import general_functions as gf

# Import specific functions for this skillset.
import functions as sf


### CONSTANTS ###
# Define program title via string.
TITLE = "Sphere Volume Calculator"

# Define program requirements via docstring.
REQUIREMENTS = """
1. Program calculates sphere volume in liquid U.S. gallons from user-entered diameter value in inches,
and rounds to two decimal places.
2. Must use Python's *built-in* PI and pow() capabilities.
3. Program checks for non-integers and non-numeric values.
4. Program continues to prompt for user entry until no longer requested, prompt accepts upper of lower case letters."""


### MAIN ###
def main():
    # Printn program header AKA Title and Requirements.
    gf.print_header(TITLE,REQUIREMENTS)
    printed_in = False
    printed_out = False
    while True:
        choice,printed_in = sf.sphere_want(printed_in)
        if choice == 'y':
            diameter,printed_out  = sf.sphere_input(printed_out)
            volume = sf.calc_volume(diameter)
            volume = sf.calc_gallons(volume)
            rounded = round(volume,2)

        
            print("\nSphere volume: {:,.2f} liquid U.S. gallons\n".format(rounded))

        else:
            print("\nThank you for using our Sphere Volume Calculator!")
            break

    
    
    
    



if __name__ == '__main__':
    main()
