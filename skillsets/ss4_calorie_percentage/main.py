"""Skillset 6"""
# Developer: Kyle Hawkins

# Course: LIS4369

# Semester: Fall 2020


### IMPORTS ###

# Import general functions for skillsets.
import general_functions as gf
# Import specific functions for this skillset.
import functions as sf


### CONSTANTS ###

# Define program title via string.
TITLE = "Calorie Percentage"
# Define program requirements via docstring.
REQUIREMENTS = """
1. Find calories per grams of fat, carbs, and protein.
2. Calculate percentages.
3. Must use float data types.
4. Format, right-align numbers, and round to two decimal places."""


### MAIN ###

def main():
    # Print program header AKA Title and Requirements.
    gf.print_header(TITLE,REQUIREMENTS)
    # Call the main content display.
    # sf.totally_not_main()
    fat, carb, protein = sf.input_grams()
    fat_cal, carb_cal, protein_cal, fat_per, carb_per, protein_per =\
        sf.calculate_calories(fat,carb,protein)
    sf.print_percentages(fat_cal, carb_cal, protein_cal, fat_per, carb_per, protein_per)

if __name__ == '__main__':
    main()
