"""Defines constants, functions, and aliases specific to Skillset 6."""

### CONSTANTS ###
CAL_PER_FAT = 9.0
CAL_PER_CARB = 4.0
CAL_PER_PROTEIN = 4.0



### FUNCTIONS ###



def input_grams():
    print("\nInput:")
    total = 0
    while total == 0:
        fat = float(input("Enter total fat grams: "))
        carb = float(input("Enter total carb grams: "))
        protein = float(input("Enter total protein grams: "))
        total = fat + carb + protein
        if total == 0:
            print ("\nFat + carb + protein cannot total zero!\n")
    return fat,carb,protein

def calculate_calories(fat,carb,protein):
    fat_cal = fat*CAL_PER_FAT
    carb_cal = carb*CAL_PER_CARB
    protein_cal = protein*CAL_PER_PROTEIN
    total_cal = fat_cal + carb_cal + protein_cal
    fat_per = fat_cal/total_cal
    carb_per = carb_cal/total_cal
    protein_per = protein_cal/total_cal
    return fat_cal, carb_cal, protein_cal, fat_per, carb_per, protein_per

def print_percentages(fat_cal, carb_cal, protein_cal, fat_per, carb_per,\
    protein_per):
    print("\nOutput:")
    print("{0:<11}{1:>8}{2:>14}".format("Type","Calories","Percentage"))
    print("{0:<11}{1:>8,.2f}{2:>14,.2%}".format("Fat",fat_cal,fat_per))
    print("{0:<11}{1:>8,.2f}{2:>14,.2%}".format("Carbs",carb_cal,carb_per))
    print("{0:<11}{1:>8,.2f}{2:>14,.2%}".format("Protein",protein_cal,protein_per))
