"""Skillset 7"""
# Developer: Kyle Hawkins

# Course: LIS4369

# Semester: Fall 2020


### IMPORTS ###
# Import general functions for skillsets.
import general_functions as gf

# Import specific functions for this skillset.
import functions as sf


### CONSTANTS ###
# Define program title via string.
TITLE = "Python Lists"

# Define program requirements via docstring.
REQUIREMENTS = """
1. Lists (Python data structure): mutable, ordered sequence of elements .
2. Lists are mut able/changeable --that is, can insert, update, delete.
3. Create list - using square brackets [list]: my_list "' ["cherries", "apples", "bananas", "oranges"].
4. Create a program that mirrors the following IPO (input/process/output) format.
Note: user enters number of requested list elements, dynamically rendered below (that is, number of elements can change each run)."""


### MAIN ###
def main():
    # Print program header AKA Title and Requirements.
    gf.print_header(TITLE,REQUIREMENTS)
    
    # Call the main content display.
    # sf.totally_not_main()
    my_list = sf.create_list()
    sf.output(my_list)

if __name__ == '__main__':
    main()
