"""
Defines functions specific to Skillset 7.
Functions:
create_list
output
"""

### CONSTANTS ###
# None

### FUNCTIONS ###
def create_list():
    """Gets size for new list then requests elements up to the size."""
    
    print("\nInput:")

    #Get list size
    list_size = int(input("Enter number of list elements:"))

    #initialize variables for loop
    i = 0
    my_list = []

    #Request elements to append to list up to list_size
    while i < list_size:
        list_element = input("Please enter list element {}: ".format(i+1))
        my_list.append(list_element)
        i+=1
    
    return my_list


def output(my_list):
    
    print("\nOutput:\nPrint my_list:")
    print(my_list)

    append_var = input("Please enter list element: ")
    append_position = int(input("Please enter list *index* position (note: must convert to int):"))
    my_list.insert(append_position,append_var)
    print("\nInsert element into specific position in my_list")
    print(my_list)

    print("\nCount number of elements in list:")
    print(len(my_list))

    print("\nSort elements in list alphabetically:")
    my_list.sort(reverse=False)
    print(my_list)

    print("\nReverse list:")
    my_list.sort(reverse=True)
    print(my_list)

    print("\nRemove last list element:")
    my_list.pop()
    print(my_list)

    print("\nDelete second element from list by *index* (note: 1=2nd element):")
    my_list.pop(1)
    print(my_list)

    print("\nDelete element from list by *value~ (cherries):")
    my_list.remove('cherries')
    print(my_list)
    
    my_list.clear()
    print("Delete all elements from list:")
    print(my_list)



### ALIASES ###
# None

