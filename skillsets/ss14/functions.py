"""Defines constants, functions, and aliases specific to Skillset 6."""

### CONSTANTS ###
OPERATORS = ['+','-','*','/','//','%','**']



### FUNCTIONS ###



def input_num1():
    while True:
        try:
            num1 = float(input("\nEnter num1: "))
            break
        except:
            print("Not valid number!")
            pass
    return num1

def input_num2():
    while True:
        try:
            num2 = float(input("\nEnter num2: "))
            break
        except:
            print("Not valid number!")
            pass
    return num2



def input_operator():
    print("\nSuitable Operators: +, -,*,/,//(integer division), % (modulo operator), ** (power)")
    operator = ''
    while True:
        try:
            operator = str(input("Enter operator: "))
            if operator not in OPERATORS:
                raise
            break
        except:
            print("\nIncorrect operator!")
            pass
    return operator

def display_result(num1,num2,operator):
    result = eval(str(num1) + operator + str(num2))
    print("{0:,.2f}".format(result))
    print("\nThank you for using our Math Calculator!")