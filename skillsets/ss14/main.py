"""Skillset 14"""
# Developer: Kyle Hawkins

# Course: LIS4369

# Semester: Fall 2020


### IMPORTS ###

# Import general functions for skillsets.
import general_functions as gf
# Import specific functions for this skillset.
import functions as sf


### CONSTANTS ###

# Define program title via string.
TITLE = "Python Calculator with Error Handling"
# Define program requirements via docstring.
REQUIREMENTS = """
1. Program calculates two numbers, and rounds to two decimal places.
2. Prompt user for two numbers, and a suitable operator.
3. Use Python error handling to validate data .
4. Test for correct arithmetic operator.
5. Division by zero not permitted.
6. Note: Program loops until correct input entered - numbers and arithmetic operator.
7. Replicate display below."""


### MAIN ###

def main():
    # Print program header AKA Title and Requirements.
    gf.print_header(TITLE,REQUIREMENTS)
    # Call the main content display.
    # sf.totally_not_main()
    #print("\nPython Calculator")
    num1 = sf.input_num1()
    operator = ''
    while True:
        try:
            num2 = sf.input_num2()
            if operator == '':
                operator = sf.input_operator()
            if num2 == 0 and operator in ['/','//','%']:
                raise
            break
        except:
            print("Cannot divide by zero!")
            pass
    
    sf.display_result(num1,num2,operator)
    
    #     print("\nCannot divide by zero!\n")
    # else:
    #    div_by_zero = False
    #sf.display_result(num1,num2,operator)


if __name__ == '__main__':
    main()
