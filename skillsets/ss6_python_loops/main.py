"""Skillset 6"""
# Developer: Kyle Hawkins

# Course: LIS4369

# Semester: Fall 2020


### IMPORTS ###

# Import general functions for skillsets.
import general_functions as gf
# Import specific functions for this skillset.
import functions as sf


### CONSTANTS ###

# Define program title via string.
TITLE = "Python Looping Structures"
# Define program requirements via docstring.
REQUIREMENTS = """
1. Print while loop.
2. Print for loops using range() function, and implicit and explicit \
lists.
3. Use break and continue statements.
4. Replicate display below.
Note: In Python, for loop used for iterating over a sequence \
(i.e., list, tuple, dictionary, set, or string)."""


### MAIN ###

def main():
    # Print program header AKA Title and Requirements.
    gf.print_header(TITLE,REQUIREMENTS)
    # Call the main content display.
    # sf.totally_not_main()
    sf.print_display()

if __name__ == '__main__':
    main()
