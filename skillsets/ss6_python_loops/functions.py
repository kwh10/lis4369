"""Defines constants, functions, and aliases specific to Skillset 6."""

### CONSTANTS ###
CONTENT = (
"while loop:",
"for loop: using range() function with 1 arg",
"for loop: using range() function with two args",
"for loop: using range() function with three args (interval 2):",
"for loop: using range() function with three args \
(negative interval):",
"for loop using (implicit) list (i.e., list not assigned to \
variable):",
"for loop iterating through (explicit) string list:",
"for loop using break statement (stops loop):",
"for loop using continue statement (stops and continues with next):",
"print list length:"
)
LIST_OF_STATES = ['Michigan','Alabama','Florida']


### FUNCTIONS ###
def loop_range(*args):
    """Solutions 2 through 5. Takes up to three args to feed to 
    range()."""
    if len(args) == 1:
        x = 0
        y, = args
        z = 1
    elif len(args) == 2:
        x,y = args
        z = 1
    elif len(args) == 3:
        x,y,z = args
    else:
        # Feels wrong to just ignore extraneous arguments.
        raise Exception("loop_range only accepts up to 3 arguments.")
    # Loops for range as long as no Exception was thrown.
    for i in range(x,y,z):
        print(i)


def loop_list(listed=[],mode='none'):
    """Solutions 6 through 10.  Toggle functionality via mode.
    Valid modes are: none, continue, break, count."""
    if len(listed) == 0:
        for i in [1, 2, 3]:
            print(i)
    elif mode == 'continue':
        n = 0
        for i in listed:
            n += 1
            if n == 2:
                continue
            print(i)
    elif mode == 'break':
        n = 0
        for i in listed:
            n += 1
            if n == 2:
                break
            print(i)
    elif mode == 'count':
        print(len(listed))
    else:
        # mode = none or invalid
        for i in listed:
            print(i)


def loop_while():
    """Solution 1.  Simple while loop."""
    i = 1
    while i <= 3:
        print(i)
        i += 1
    del i


def print_content_line(number):
    """Prints content line from CONTENT tuple based on number."""
    print("\n{}. ".format(number) + CONTENT[number-1])
    

def print_display():
    """Displays the main body of the assignment using an outer loop 
    to iterate through function calls and the content tuple.
    It is intentionally over the top for such a simple program."""
    i = 0
    while i <10:
        i += 1
        print_content_line(i)
        if i == 1:
            loop_while()
        elif i == 2:
            loop_range(4)
        elif i == 3:
            loop_range(1,4)
        elif i == 4:
            loop_range(1,4,2)
        elif i == 5:
            loop_range(3,0,-2)
        elif i == 6:
            loop_list()
        elif i == 7:
            loop_list(LIST_OF_STATES)
        elif i == 8:
            loop_list(LIST_OF_STATES,'break')
        elif i == 9:
            loop_list(LIST_OF_STATES,'continue')
        elif i == 10:
            loop_list(LIST_OF_STATES,'count')

### ALIASES ###
# This one is just for laughs.
totally_not_main = print_display
