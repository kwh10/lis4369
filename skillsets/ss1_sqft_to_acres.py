#!/usr/bin/env python3

# Developer: Kyle Hawkins

# Course: LIS4369

# Semester: Fall 2020

import general_functions as gf
import ss1_functions as sf

#Define title and requirements.50
TITLE="Square Feet to Acres"
REQUIREMENTS = """
1. Research: number of square feet to acre of land.
2. Must use float data type for user input and calculation.
3. Format and round conversion to two decimal places."""

def main():
    # Print Title and requirements.
    gf.print_requirements(TITLE,REQUIREMENTS)
    sqft = sf.input_sqft()
    acre = sf.sqft_to_acre(sqft)
    sf.print_sqft_acre(sqft,acre)

if __name__ == '__main__':
    main()