#!/usr/bin/env python3

# Developer: Kyle Hawkins

# Course: LIS4369

# Semester: Fall 2020

import general_functions as gf
import ss3_functions as sf

#Define title and requirements.
TITLE="IT/ICT Student Percentage"
REQUIREMENTS = """
1. Find number of IT/ICT students in class.
2. Calculate IT/ICT Student Percentage.
3. Must use float data type (to facilitate right-alignment).
4. Format, right-align numbers, and round to two decimal places."""

def main():
    # Print Title and requirements.
    gf.print_requirements(TITLE,REQUIREMENTS)
    students_it, students_ict = sf.input_students()
    students_it_percent, students_ict_percent, students_total\
        = sf.calculate_students_percent(students_it,students_ict)
    sf.print_students_percent(students_it_percent,
        students_ict_percent, students_total)

if __name__ == '__main__':
    main()