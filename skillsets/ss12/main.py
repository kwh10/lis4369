"""Skillset 12"""
# Developer: Kyle Hawkins

# Course: LIS4369

# Semester: Fall 2020


### IMPORTS ###
# Import general functions for skillsets.
import general_functions as gf

# Import specific functions for this skillset.
import functions as sf


### CONSTANTS ###
# Define program title via string.
TITLE = "Temperature Conversion Program"

# Define program requirements via docstring.
REQUIREMENTS = """
1. Program converts user-entered temperatures into Fahrenheit or Celsius scales.
2. Program continues to prompt for user entry until no longer requested.
3. Note: upper or lower case letters permitted. Though, incorrect entries are not permitted.
4. Note: Program does not va lidate numeric data (optional requirement}."""


### MAIN ###
def main():
    # Print program header AKA Title and Requirements.
    gf.print_header(TITLE,REQUIREMENTS)
    while True:
        choice = sf.temp_want()
        if choice == 'y':
            temp,unit = sf.temp_input()
            sf.temp_conversion(temp,unit)
        else:
            print("\nThank you for using our Temperature Conversion Program!")
            break
    #sf.temp_conversion()



if __name__ == '__main__':
    main()
