"""
Defines functions specific to Skillset 12.
Functions:
random_input
random_randint
random_shuffle
"""

### CONSTANTS ###
# None

### FUNCTIONS ###
def temp_want():
    print("\nInput: ")
    while True:
        try:
            choice = input("Do you want to convert a temperature (y or n)?").lower()
            if choice == 'y' or choice == 'n':
                break
            else:
                raise 'incorrect'
        except:
            print("Incorrect entry. Please try again.\n")
            pass
    return choice

def temp_input():
    while True:
        try:
            unit = input("Fahrenheit to Celsius? Type \"f\", or Celsius to Fahrenheit? Type \"c\":").lower()
            if unit == 'f' or unit == 'c':
                break
            else:
                raise 'incorrect'
        except:
            print("Incorrect entry. Please try again.\n")
            pass

    if unit == 'f':
        word = "Fahrenheit"
    else:
        word = "Celsius"
    
    while True:
        try:
            temp = float(input("Enter temperature in {}:".format(word)))
            if True:
                break
            else:
                raise 'incorrect'
        except:
            print("Incorrect entry. Please try again.\n")
            pass
    return temp,unit

def temp_conversion(temp,unit):
    print("\nOutput: ")

    if unit == 'f':
        temp = ((temp - 32)*5)/9
        print("Temperature in Celsius = " + str(temp))

    else:
        temp = (temp *9/5) + 32
        print("Temperature in Fahrenheit = " + str(temp))

### ALIASES ###
# None

