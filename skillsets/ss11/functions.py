"""
Defines functions specific to Skillset 11.
Functions:
random_input
random_randint
random_shuffle
"""
import random
### CONSTANTS ###
# None

### FUNCTIONS ###
def random_input():
    print("\nInput:")
    start = int(input("Enter beginning value: "))
    end = int(input("Enter ending value: "))
    return start, end

def random_randint(start, end):
    ran_num = random.randint(start,end)
    return ran_num

def random_shuffle(start, end):
    ran_list = []
    ran_list.extend(range(start,end))
    random.shuffle(ran_list)
    return ran_list.pop()

### ALIASES ###
# None

