"""Skillset 11"""
# Developer: Kyle Hawkins

# Course: LIS4369

# Semester: Fall 2020


### IMPORTS ###
# Import general functions for skillsets.
import general_functions as gf

# Import specific functions for this skillset.
import functions as sf


### CONSTANTS ###
# Define program title via string.
TITLE = "Pseudo-Random Number Generator"

# Define program requirements via docstring.
REQUIREMENTS = """
1. Get user beginning and ending integer values, and store in two variables.
2. Display 10 pseudo-random numbers between, and including, above values.
3. Must use integer data types.
4. Example 1: Using range() and randint() functions.
5. Example 2: Using a list with range() and shuffle() functions."""


### MAIN ###
def main():
    # Print program header AKA Title and Requirements.
    gf.print_header(TITLE,REQUIREMENTS)
    

    start,end  = sf.random_input()
    
    print("\nOutput:")
    print("Example 1: Using range() and randint() functions:")

    count = 0
    while count < 10:
        print(sf.random_randint(start,end), end = ' ')
        count += 1

    print("\n\nExample 2: Using range() and shuffle() functions:")

    count = 0
    while count < 10:
        print(sf.random_shuffle(start,end), end = ' ')
        count += 1



if __name__ == '__main__':
    main()
