#!/usr/bin/env python3

# Developer: Kyle Hawkins

# Course: LIS4369

# Semester: Fall 2020

import general_functions as gf
import ss2_functions as sf

#Define title and requirements.50
TITLE="Miles Per Gallon"
REQUIREMENTS = """
1. Convert MPG.
2. Must use float data type for user input and calculation.
3. Format and round conversion to two decimal places."""

def main():
    # Print Title and requirements.
    gf.print_requirements(TITLE,REQUIREMENTS)
    miles, gallons = sf.input_data()
    mpg = sf.calculate_mpg(miles,gallons)
    sf.print_mpg(miles,gallons,mpg)

if __name__ == '__main__':
    main()