"""Defines functions and constants specific to Skillset 1

"""
FT_PER_ACRE = 43560


def input_sqft():
    print("\nInput:")
    sqft = float(input("Enter square feet: "))
    return sqft
def sqft_to_acre(sqft):
    acre = sqft / FT_PER_ACRE
    return acre
def print_sqft_acre(sqft,acre):
    print("\nOutput:")
    print("{0:,.2f} square feet = {1:,.2f} acres".format(sqft,acre))