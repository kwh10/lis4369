"""Defines constants, functions, and aliases specific to Skillset 6."""

### CONSTANTS ###
OPERATORS = ['+','-','*','/','//','%','**']



### FUNCTIONS ###



def input_numbers():
    num1 = float(input("Enter num1: "))
    num2 = float(input("Enter num2: "))
    return num1, num2
def input_operator():
    print("\nSuitable Operators: +, -,*,/,//(integer division), % (modulo operator), ** (power)")
    operator = ''
    while operator not in OPERATORS:
        operator = str(input("Enter operator: "))
        if operator not in OPERATORS:
            print("Incorrect operator!")
    return operator
def display_result(num1,num2,operator):
    result = eval(str(num1) + operator + str(num2))
    print("{0:,.1f}".format(result))