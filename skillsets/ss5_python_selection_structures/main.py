"""Skillset 6"""
# Developer: Kyle Hawkins

# Course: LIS4369

# Semester: Fall 2020


### IMPORTS ###

# Import general functions for skillsets.
import general_functions as gf
# Import specific functions for this skillset.
import functions as sf


### CONSTANTS ###

# Define program title via string.
TITLE = "Python Selection Structures"
# Define program requirements via docstring.
REQUIREMENTS = """
1. Use Python selection structure.
2. Prompt user for two numbers, and a suitable operator.
3. Test for correct numeric operator.
4. Replicate display below."""


### MAIN ###

def main():
    # Print program header AKA Title and Requirements.
    gf.print_header(TITLE,REQUIREMENTS)
    # Call the main content display.
    # sf.totally_not_main()
    print("\nPython Calculator")
    div_by_zero = True
    while div_by_zero == True:
        num1,num2 = sf.input_numbers()
        operator = sf.input_operator()
        if num2 == 0 and operator in ['/','//','%']:
            print("\nCannot divide by zero!\n")
        else:
            div_by_zero = False
    sf.display_result(num1,num2,operator)


if __name__ == '__main__':
    main()
