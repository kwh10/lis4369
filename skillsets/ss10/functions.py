"""
Defines functions specific to Skillset 11.
Functions:
random_input
random_randint
random_shuffle
"""

### CONSTANTS ###
# None

### FUNCTIONS ###
def input_dict():
    my_dictionary = {}

    print("\nInput:")
    var = input("First Name: ")
    my_dictionary['fname'] = var

    var = input("Last Name: ")
    my_dictionary['lname'] = var

    var = input("Degree: ")
    my_dictionary['degree'] = var

    var = input("Major (IT or ICT): ")
    my_dictionary['major'] = var

    var = input("GPA: ")
    my_dictionary['gpa'] = var

    return my_dictionary

def output_dict(my_dictionary):
    print("\n\nOutput:")

    print("\nPrint my_dictionary:")
    print(my_dictionary)

    print("\nReturn view of dictionary's (key, value) pair, built-in function:")
    print(my_dictionary.items())

    print("\nReturn view object of all keys, built-in function:")
    print(my_dictionary.keys())

    print("\nReturn view object of all values in dictionary, built-in function:")
    print(my_dictionary.values())

    print("\nPrint only first and last names, using keys:")
    print(my_dictionary['fname'],my_dictionary['lname'])

    print("\nPrint only first and last names, using get() function:")
    print(my_dictionary.get('fname'),my_dictionary.get('lname'))

    print("\nCount number of items (key:value pairs) 1n dictionary:")
    print(len(my_dictionary))

    print("\nRemove last dictionary item (popitem):")
    my_dictionary.popitem()
    print(my_dictionary)

    print("\nDelete major from dictionary, using key:")
    my_dictionary.pop('major', None)
    print(my_dictionary)

    print("\nReturn object type:")
    print(type(my_dictionary))

    print("\nDelete all items from list:")
    my_dictionary.clear()
    print(my_dictionary)

### ALIASES ###
# None

