"""Skillset 10"""
# Developer: Kyle Hawkins

# Course: LIS4369

# Semester: Fall 2020


### IMPORTS ###
# Import general functions for skillsets.
import general_functions as gf

# Import specific functions for this skillset.
import functions as sf


### CONSTANTS ###
# Define program title via string.
TITLE = "Python Dictionaries"

# Define program requirements via docstring.
REQUIREMENTS = """
1. Dictionaries (Python data structure): unordered key:value pairs.
2. Dictionary: an associative array (also known as hashes).
3. Any key in a dictionary is associated (or mapped) to a value (i.e., any Python data type).
4. Keys: must be of irmu.rtable type (string, number or tuple with immutable elements) and must be unique.
5. Values: can be any data type and can repeat.
6. Create a program that mirrors the following IPO (input/process/output) format.
    Create empty dictionary, using curly braces \\{\\}: my_dictionary = {}
    Use the following keys: fname, lname, degree, major, gpa
Note: Dictionaries have key-value pairs instead of single values; this differentiates a dictionary from a set."""


### MAIN ###
def main():
    # Print program header AKA Title and Requirements.
    gf.print_header(TITLE,REQUIREMENTS)

    # Create dict from user input
    my_dictionary = sf.input_dict()

    # No real calculation on this one

    # Output
    sf.output_dict(my_dictionary)



if __name__ == '__main__':
    main()
