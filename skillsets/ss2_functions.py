"""Defines functions and constants specific to Skillset 1

"""


def input_data():
    print("\nInput:")
    miles = float(input("Enter miles driven: "))
    gallons = 0
    while gallons == 0: 
        gallons = float(input("Enter gallons of fuel used: "))
    return miles, gallons
def calculate_mpg(miles,gallons):
    mpg = miles / gallons
    return mpg
def print_mpg(miles,gallons,mpg):
    print("\nOutput:")
    print("{0:,.2f} miles driven and {1:,.2f} gallons used = {2:,.2f} mpg"
        .format(miles,gallons,mpg))