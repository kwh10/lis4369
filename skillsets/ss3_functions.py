"""Defines functions and constants specific to Skillset 1

"""


def input_students():
    print("\nInput:")
    students_it = 0
    students_ict = 0
    while students_it + students_ict == 0: 
        students_it = float(input("Enter number of IT students: "))
        students_ict = float(input("Enter number of ICT students: "))
        if students_it + students_ict == 0:
            print("Total students cannot equal 0.")
    return students_it, students_ict
def calculate_students_percent(students_it,students_ict):
    students_total = students_it + students_ict
    students_it_percent = students_it / students_total
    students_ict_percent = students_ict / students_total
    return students_it_percent, students_ict_percent, students_total
def print_students_percent(students_it_percent, students_ict_percent, students_total):
    print("\nOutput:")
    print("{0:17} {1:>5.2f}".format("Total students:", students_total))
    print("{0:17} {1:>5.2%}".format("IT Students:", students_it_percent))
    print("{0:17} {1:>5.2%}".format("ICT Students:", students_ict_percent))
