# LIS 4369 - Extensible Enterprise Solutions

## Kyle Hawkins

### Skillset 7 Requirements:

* Skillsets Links:
    * [general_functions.py](../skillsets/general_functions.py)
    * 4 - Calorie Percentage
        * [main.py](../skillsets/ss4_calorie_percentage/main.py)
        * [functions.py](../skillsets/ss4_calorie_percentage/functions.py)
    * 5 - Python selection Structures
        * [main.py](../skillsets/ss5_python_selection_structures/main.py)
        * [functions.py](../skillsets/ss5_python_selection_structures/functions.py)
    * 6 - Python Loops
        * [main.py](../skillsets/ss6_python_loops/main.py)
        * [functions.py](../skillsets/ss6_python_loops/functions.py)



#### Skillset Screenshots:

*Skillset 7*:

![Skillset 6 - Python Loops](../skillsets/img/ss7.png)