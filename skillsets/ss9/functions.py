"""
Defines functions specific to Skillset 7.
Functions:
create_list
output
"""

### CONSTANTS ###
# None

### FUNCTIONS ###

def output():
    
    print("\nInput: Hard coded--no user Input. See three examples above.")
    print("***Note***: All three sets below print as \"sets\" (i.e., curly brackets), *regardless• of how they were created)")
    
    print("\nPrint my_set created using curly brackets:")
    my_set = {1, 3.14, 2.0, 'four', 'Five'}
    print(my_set)

    print("\nPrint type of my set:")
    print(type(my_set))

    print("\nPrint my_setl created using set() function with list:")
    my_set1 = set([1, 3.14, 2.0, 'four', 'Five'])
    print(my_set1)

    print("\nPrint type of my_set1:")
    print(type(my_set1))

    print("\nPrint my_set2 created using set() function with tuple:")
    my_tuple = (1, 3.14, 2.0, 'four', 'Five')
    my_set2 = set(my_tuple)
    print(my_set2)

    print("\nPrint type of my_set2:")
    print(type(my_set2))

    print("\nLength of my_set:")
    print(len(my_set))

    print("\nDiscard 'four':")
    my_set.discard('four')
    print(my_set)

    print("\nRemove 'Five':")
    my_set.remove('Five')
    print(my_set)

    print("\nLength of my_set:")
    print(len(my_set))

    print("\nAdd element to set (4) using add() method:")
    my_set.add(4)
    print(my_set)

    print("\nLength of my_set:")
    print(len(my_set))

    print("\nDisplay minimum number:")
    print(min(my_set))

    print("\nDisplay maximum number:")
    print(max(my_set))

    print("\nDisplay sum of numbers:")
    print(sum(my_set))

    print("\nDelete all set elements:")
    my_set.clear()
    print(my_set)

    print("\nLength of my_set:")
    print(len(my_set))

### ALIASES ###
# None

