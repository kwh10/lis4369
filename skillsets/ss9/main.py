"""Skillset 9"""
# Developer: Kyle Hawkins

# Course: LIS4369

# Semester: Fall 2020


### IMPORTS ###
# Import general functions for skillsets.
import general_functions as gf

# Import specific functions for this skillset.
import functions as sf


### CONSTANTS ###
# Define program title via string.
TITLE = "Python Sets - like mathematical sets!"

# Define program requirements via docstring.
REQUIREMENTS = """
1. Sets (Python data structure): mutable, heterogeneous, unordered sequence of elements, *but'" cannot hold duplicate values.
2. Sets are mutable/changeable--that is, can Insert, update, delete.
3. While sets are mutable/changeable, they *cannot* contain other mutable Items like list, set, or dictlonary-that
is, elements contained In set must be Immutable.
4. Also, since sets are unordered, cannot use indexing (or, slicing) to access, update, or delete elements.
5. Two methods to create sets:
a. Create set using curly brackets \{set\}: my set= {1, 3.14, 2.0, 'four', 'Five'}
b. Create set using set() function: my set= set( <lterable>)
Examples:
my setl = set([l, 3.14, 2.0, 'four', 'Five']) # set with lisit
my_set2 = set((l, 3.14, 2.0, 'four', 'Five'))# set with tuple
5. Note: An "iterable" Is *any"" object, which can be iterated over--that is, lists, tuples, or even strings.
6. Create a program that mirrors the following IPO (input/process/output) format."""


### MAIN ###
def main():
    # Print program header AKA Title and Requirements.
    gf.print_header(TITLE,REQUIREMENTS)
    
    # Call the main content display.
    # sf.totally_not_main()
    sf.output()

if __name__ == '__main__':
    main()
