"""
Defines functions specific to Skillset 8.
Functions:
create_list
output
"""

### CONSTANTS ###
# None

### FUNCTIONS ###
def output():
    print("\033[94mInput\033[37m: Hard coded--no input.")

    my_tuple1 = ('cherries','apples','bananas','oranges')

    my_tuple2 = (1,2,'three','four')

    print("\n\033[94mOutput\033[37m:\nPrint my_tuple1:")
    print(my_tuple1)

    print("\nPrint my_tuple2:")
    print(my_tuple2)

    print("\nPrint my_tuple1 unpacking:")
    a,b,c,d = my_tuple1
    print(a,b,c,d)

    print("\nPrint third element in my_tuple2:")
    print(my_tuple2[2])

    print("\nPrint \"slice\" of my_tuple1 (second and third elemnts):")
    print(my_tuple1[1:-1])

    print("\nReassign my_tuple2 using parenthesis.")
    my_tuple2 = (1,2,3,4)
    print (my_tuple2)

    print("\nReassign my_tuple2 using \"packing\" method (no parenthasis).")
    my_tuple2 = 5,6,7,8
    print(my_tuple2)

    print("\nPrint numer of elements in my_tuple1:", len(my_tuple1))

    print("Print type of my_tuple1:", type(my_tuple1))

    print("Delete my_tuple1:")
    del my_tuple1
    print("\033[94mNote\033[37m: will generate error, if trying to print after, as it no longer exists.")
### ALIASES ###
# None

