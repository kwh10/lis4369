"""Skillset 8"""
# Developer: Kyle Hawkins

# Course: LIS4369

# Semester: Fall 2020


### IMPORTS ###
# Import general functions for skillsets.
import general_functions as gf

# Import specific functions for this skillset.
import functions as sf


### CONSTANTS ###
# Define program title via string.
TITLE = "\033[37mPython Tuples"

# Define program requirements via docstring.
REQUIREMENTS = """
1. Tuples (Python data structure): *immutable* (cannot be changed!), ordered sequence of elements.
2. Tuples are immutable/unchangeable--that is, cannot insert, update, delete.
\033[94mNote\033[37m: can reassign or delete an *entire* tuple--but, *not* individual items or slices.
3. Create tuple using parentheses (tuple): my_tuplel = ("cherries", "apples", "bananas", "oranges")
4. Create tuple (packing), that is, *without* using parentheses (aka tuple "packing"): my_tuple2 = 1, 2, "three", "four"
5. Python tuple (unpacking), that is, assign values from tuple to sequence of variables: fruitl, fruit2, fruit3, fruit4 = my_tuplel
6. Create a program that mirrors the following IPO (input/process/output) format."""


### MAIN ###
def main():
    # Print program header AKA Title and Requirements.
    gf.print_header(TITLE,REQUIREMENTS)
    
    # Call the main content display.
    sf.output()

if __name__ == '__main__':
    main()
