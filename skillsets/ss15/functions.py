"""
Defines functions specific to Skillset 15.
"""
import pathlib

GETTYSBURG_ADDRESS = """
President Abraham Lincoln's Gettysburg Address:
Four score and seven years ago our fathers brought forth on this continent, a new nation, conceived in Liberty, and dedicated to the
proposition that all men are created equal.
Now we are engaged in a great civil war, testing whether that nation, or any nation so conceived and so dedicated, can long endure.
We are met on a great battle-field of that war. We have come to dedicate a portion of that field, as a final resting place for those who
 here gave their lives that that nation might live. It is altogether fitting and proper that we should do this.
But, in a larger sense, we can not dedicate -- we can not consecrate -- we can not hallow - - this ground. The brave men, living and d
ead , who struggled here, have consecrated it, far above our poor power to add or detract. The world will little note, nor long rememb
er what we say here, but it can never forget what they did here. lit is for us the liv ing, rather, to be dedicated here to the unfinished
work which they who fought here have thus far so nobly advanced. It is rather for us to be here dedicated to the great task remain in
g before us -- that from these honored dead we take increased devotion to that cause for which they gave the last fu ll measure of de
votion -- that we here highly resolve that these dead shall not have died in vain -- that this nation, under God, shall have a new birt
h of freedom -- and that government of the people, by the people, for the people, shall not perish from the earth.

Abraham Lincoln
November 19, 1863
"""
### FUNCTIONS ###
def write_read_file():
    """
    Usage: Calls two functions:
    1. file_ write() # writes to file
    2. file_read() # reads from file
    Parameters: none
    Returns: none
    """
    file_write()
    file_read()
    return

def file_write():
    """
    Usage: creates file, and writes contents of global variable to file
    Parameters: none
    Returns: none
    """
    f = open("test.txt","w")
    f.write(GETTYSBURG_ADDRESS)
    f.close()

    return

def file_read():
    """
    Usage: reads contents of written file
    Parameters: none
    Returns: none
    """
    f = open("test.txt", "r")
    print (f.read())

    print("Full file path:")
    print(str(pathlib.Path(__file__).parent.absolute()) + "\\test.txt")

    return

### ALIASES ###
# None

