"""Skillset 15"""
# Developer: Kyle Hawkins

# Course: LIS4369

# Semester: Fall 2020


### IMPORTS ###
# Import general functions for skillsets.
import general_functions as gf

# Import specific functions for this skillset.
import functions as sf


### CONSTANTS ###
# Define program title via string.
TITLE = "File Write Read"

# Define program requirements via docstring.
REQUIREMENTS = """
1. Create write_read_file subdirectory with two files: main.py and functions.py.
2. Use President Abraham Lincoln's Gettysburg Address: Full Text.
3. Write address to file.
4. Read address from same file.
5. Create Python Docstrings for functions in functions.py file.
6. Display Python Docstrings for each function in functions.py file.
7. Display full file path.
8. Replicate display below.
"""


### MAIN ###
def main():
    # Printn program header AKA Title and Requirements.
    gf.print_header(TITLE,REQUIREMENTS)

    help(sf.write_read_file)
    help(sf.file_write)
    help(sf.file_read)

    sf.write_read_file()

    
    
    
    



if __name__ == '__main__':
    main()
