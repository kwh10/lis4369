# LIS 4369 - Extensible Enterprise Solutions

## Kyle Hawkins

### Project 2 Requirements:

*Two Parts:*

1. Referencing Assignment 5 screenshots as examples:
    * Backward-engineer the lis4369_p2_requirements.txt file
    * Include two plots
2. Test with RStudio

#### README.md file should include the following items:

* P2 Links:
    * [lis4369_p2.R](p2/lis4369_p2.R)
    * [lis4369_p2_requirements.txt](p2/lis4369_p2_requirements.txt)
* P2 Screenshots:
    * P2 - 4 parts

----

#### Project Screenshots:

*P2 Output Part 1*:

![P2 Output Part 1](img/p2_output_1.png)

*P2 Output Part 2*:

![P2 Output Part 2](img/p2_output_2.png)

*P2 Qplot*:

![P2 Qplot](img/p2_qplot.png)

*P2 Plot*:

![P2 Plot](img/p2_plot.png)