"""Defines six functions:

1. estimate_painting_cost()
2. input_paint_specs()
3. calculate_painting_cost(sqft,price_gallon,price_labor)
4. print_output(sqft,gallons,price_gallon,price_labor,cost_labor,\
    cost_paint,cost_total)
5. print_painting_estimate(title,value)
6. print_painting_percentage(value,total)

Defines one constant:
SQFT_PER_GALLON"""

SQFT_PER_GALLON = 350

def estimate_painting_cost():
    """Outer loop, displays main \"menu.\""""
    another = 'y'
    while another == 'y':
        sqft, price_gallon, price_labor = input_paint_specs()
        gallons,cost_paint,cost_labor,cost_total\
            = calculate_painting_cost(sqft, price_gallon, price_labor)
        print_output(sqft,gallons,price_gallon,price_labor,cost_labor,\
            cost_paint,cost_total)
        
        #Another y/n loop.
        another = ''
        while another != 'n' and another != 'y':
            another = str(input('\nEstimate another paint job? (y/n): '))
    print("\nThank you for using our Painting Estimator!")
    print("Please see our web site: http://www.mysite.com")


def input_paint_specs():
    """Accepts 0 args. Gets paint job specs from user.
    Returns sqft, price_gallon, price_labor"""
    print("\nInput:")
    sqft = 0
    while sqft == 0:
        sqft = float(input("Enter total interior sq ft: "))
        if sqft == 0:
            print("\nSq Ft cannot equal zero!\n")
    price_gallon = 0
    price_labor = 0
    while price_gallon == 0 and price_labor == 0:
        price_gallon = float(input("Enter price per gallon paint: "))
        price_labor = float(input("Enter hourly painting rate per sq ft: "))
        if price_gallon == 0 and price_labor == 0:
            print("\nEither price per gallon or hourly rate must not\
                 equal zero!\n")
    return sqft,price_gallon,price_labor


def calculate_painting_cost(sqft,price_gallon,price_labor):
    """Calculates paint job costs based on four arguments"""
    gallons = sqft/SQFT_PER_GALLON
    cost_paint = gallons * price_gallon
    cost_labor = sqft * price_labor
    cost_total = cost_paint + cost_labor
    return gallons,cost_paint,cost_labor,cost_total


def print_output(sqft,gallons,price_gallon,price_labor,cost_labor,\
    cost_paint,cost_total):
    """Handles output, calls other print functions in loops"""
    print ("\nOutput:")
    print ("{0:<20} {1:>9}".format('Item', 'Amount'))
    print ("{0:<20} {1:>9,.2f}".format('Total Sq Ft:', sqft))
    print ("{0:<20} {1:>9,.2f}".format('Sq Ft per Gallon:', SQFT_PER_GALLON))
    print ("{0:<20} {1:>9,.2f}".format('Number of Gallons:', gallons))
    print ("{0:<20} ${1:>8,.2f}".format('Paint per Gallon:', price_gallon))
    print ("{0:<20} ${1:>8,.2f}".format('Labor per Sq Ft:', price_labor))

    print("\n{0:<11} {1:<9} {2:>10}".format('Cost', 'Amount', 'Percentage'))
    title = ['Paint:','Labor:','Total']
    value = [cost_paint,cost_labor,cost_total]
    i = 0
    while i < 3:
        print_painting_estimate(title[i],value[i])
        print_painting_percentage(value[i],cost_total)
        i+=1


    pass

def print_painting_estimate(title,value):
    """Prints the Cost table and non-percentage values."""
    print ("{0:<8} ${1:>8,.2f}".format(title, value), end='')


def print_painting_percentage(value,total):
    """Just displays the percentages per requirements."""
    print ("{0:>14,.2%}".format(value/total))