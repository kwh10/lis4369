"""Assignment 3 - Painting Estimator"""
# Developer: Kyle Hawkins

# Course: LIS4369

# Semester: Fall 2020

import functions as f
import general_functions as gf

TITLE = "Painting Estimator"
REQUIREMENTS = """Program Requirements:
1. Calculate home interior paint cost (w/o primer).
2. Must use float data types.
3. Must use SQFT_PER_GALLON constant (350).
4. Must use iteration structure (aka "loop").
S. Format, right-align numbers, and round to two decimal places.
6. Create at least five functions that are called by the program:
        a. main(): calls two other functions: get_requirements() and estimate_painting_cost()
        b. get_requirements(): displays the program requirements.
        c. estimate_painting_cost(): calculates interior home painting, and calls print functions.
        d. print_painting_estimate(): displays painting costs.
        e. print_painting_percentage(): displays painting costs percentages."""

def main():
    # Print Title and requirements.
    gf.get_requirements(TITLE,REQUIREMENTS,('',))
    # Following function has to do pretty much everything to adhere to
    #   it's function requirements
    f.estimate_painting_cost()


if __name__ == '__main__':
    main()