# LIS 4369 - Extensible Enterprise Solutions

## Kyle Hawkins

### Assignment 3 Requirements:

*Four Parts:*

1. Backward-Engineer Painting Estimator.
2. Two modules:
    * functions.py containing:
        * get_requirements()
        * estimate_painting_cost()
        * print_painting_estimate()
        * print_painting_percentage()
    * main.py module imports the functions.py module, and calls the functions.
3. Test and provide screenshots for IDLE and VSC.
4. Skillsets 4, 5, and 6.

#### README.md file should include the following items:

* A3 Links:
    * [main.py](a3_painting_estimator/main.py)
    * [functions.py](a3_painting_estimator/functions.py)
    * [general_functions.py](a3_painting_estimator/general_functions.py)
    * [payroll_calculator.ipynb](a3_painting_estimator/payroll_calculator.ipynb)
* A3 Screenshots:
    * VSC with and without overtime
    * IDLE with and without overtime
    * Jupyter notebook

* Skillsets Links:
    * [general_functions.py](../skillsets/general_functions.py)
    * 4 - Calorie Percentage
        * [main.py](../skillsets/ss4_calorie_percentage/main.py)
        * [functions.py](../skillsets/ss4_calorie_percentage/functions.py)
    * 5 - Python selection Structures
        * [main.py](../skillsets/ss5_python_selection_structures/main.py)
        * [functions.py](../skillsets/ss5_python_selection_structures/functions.py)
    * 6 - Python Loops
        * [main.py](../skillsets/ss6_python_loops/main.py)
        * [functions.py](../skillsets/ss6_python_loops/functions.py)
* Skillset Screenshots:
    * Calorie Percentage
    * Python Selection Structures
    * Python Loops

----

#### Assignment Screenshots:

*VSC Painting Estimator*:

![VSC Painting Estimator](img/vsc.jpg)

*IDLE Painting Estimator*:

![IDLE Painting Estimator](img/idle.jpg)

*Jupyter Notebook General Functions*:

![Jupyter Notebook General Functions](img/jupyter_notebook_general_functions.jpg)

*Jupyter Notebook Functions*:

![Jupyter Notebook Functions](img/jupyter_notebook_functions.jpg)

*Jupyter Notebook Main*:

![Jupyter Notebook Main](img/jupyter_notebook_main.jpg)

----

#### Skillset Screenshots:
*Skillset 4 - Calorie Percentage*:

![Skillset 4 - Calorie Percentage](../skillsets/img/ss4_calorie_percentage.jpg)

*Skillset 5 - Python Selection Structures*:

![Skillset 5 - Python Selection Structures](../skillsets/img/ss5_python_selection_structures.jpg)

*Skillset 6 - Python Loops*:

![Skillset 6 - Python Loops](../skillsets/img/ss6_python_loops.jpg)