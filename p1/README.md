# LIS 4369 - Extensible Enterprise Solutions

## Kyle Hawkins

### Assignment 3 Requirements:

*Four Parts:*

1. Code and run demo.py
2. Backward engineer provided screenshot.
3. Test and provide screenshots for IDLE and VSC.
4. Skillsets 7, 8, and 9.

#### README.md file should include the following items:

* P1 Links:
    * [main.py](p1_data_analysis/main.py)
    * [functions.py](p1_data_analysis/functions.py)
    * [general_functions.py](p1_data_analysis/general_functions.py)
    * [payroll_calculator.ipynb](p1_data_analysis/p1_data_analysis.ipynb)
* P1 Screenshots:
    * VSC - 3 parts
    * IDLE - 2 parts
    * Jupyter notebook - 3 parts
    * Figure - 1 Part

* Skillsets Links:
    * [general_functions.py](../skillsets/general_functions.py)
    * 7 - Using Lists
        * [main.py](../skillsets/ss7/main.py)
        * [functions.py](../skillsets/ss7/functions.py)
    * 8 - Using Tuples
        * [main.py](../skillsets/ss8/main.py)
        * [functions.py](../skillsets/ss8/functions.py)
    * 9 - Using Sets
        * [main.py](../skillsets/ss9/main.py)
        * [functions.py](../skillsets/ss9/functions.py)
* Skillset Screenshots:
    * Using Lists - 2 Parts
    * Using Tuples - 1 Part
    * Using Sets - 2 Parts

----

#### Assignment Screenshots:

*Project 1 VSC-1*:

![Project 1 VSC-1](img/VSC_1.png)

*Project 1 VSC-2*:

![Project 1 VSC-2](img/VSC_2.png)

*Project 1 VSC-3*:

![Project 1 VSC-3](img/VSC_3.png)

*Project 1 VSC-3*:

![Project 1 VSC-3](img/VSC_3.png)

*Project 1 IDLE-1*:

![Project 1 IDLE-1](img/idle_1.png)

*Project 1 IDLE-2*:

![Project 1 IDLE-2](img/idle_2.png)

*Jupyter Notebook Part 1*:

![Jupyter Notebook Part 1](img/p1_data_analysis_jupyter_Page_1.png)

*Jupyter Notebook Part 2*:

![Jupyter Notebook Part 2](img/p1_data_analysis_jupyter_Page_2.png)

*Jupyter Notebook Part 3*:

![Jupyter Notebook Part 3](img/p1_data_analysis_jupyter_Page_3.png)

*Project 1 Figure*:

![Project 1 Figure](img/figure.png)

----

#### Skillset Screenshots:
*Skillset 7 Part 1*:

![Skillset 7 Part 1](../skillsets/img/ss7.png)

*Skillset 7 Part 2*:

![Skillset 7 Part 2](../skillsets/img/ss7-2.png)

*Skillset 8*:

![Skillset 8](../skillsets/img/ss8.png)

*Skillset 9 Part 1*:

![Skillset 9 Part 1](../skillsets/img/ss9-1.png)

*Skillset 9 Part 2*:

![Skillset 9 Part 2](../skillsets/img/ss9-2.png)