"""LIS 4369 functions module.  Defines general use functions."""

### CONSTANTS ###
DEV_NAME = "Kyle Hawkins"


### Functions ###
def print_header(title,requirements,developers=(DEV_NAME,)):
    """Takes title string, requirements docstring, and optional
    developers tuple.  Formats and prints the arguments per LIS4369 
    skillsets specification.
    Returns nothing."""
    if(len(developers) > 1):
        prefix="Developers"
        seperator = ", "
        body = seperator.join(developers[:1]) + ", and " + developers[-1]
        print("{}: ".format(prefix) + body)
    elif developers[0] == '':
        pass
    else:
        prefix="Developer"
        body = developers[0]
        print("{}: ".format(prefix) + body)
    
    print(title
    + "\n\nProgram Requirements:"
    + requirements)


### ALIASES ###
# For backwards compatibility.
print_requirements = print_header
get_requirements = print_header