"""Project 1 - Data Analysis 1"""
# Developer: Kyle Hawkins

# Course: LIS4369

# Semester: Fall 2020

import functions as f
import general_functions as gf


TITLE = "Data Analysis 1"
REQUIREMENTS = """Program Requirements:
1. Run demo.py.
2. If errors, more than likely missing installations.
3. Test Python Package Installer: pip freeze
4. Research how to do the following installations:
    a. pandas (only if missing)
      
    b. pandas-datareader (only if missing)
    c. matplotlib (only if missing)
5. Create at least three functions that are called by the program:
    a. main(): calls at least two other functions.
    a. get_requirements(): displays the program requirements.
    c. data_analysis_l(): displays the following data."""

def main():
    # Print Title and requirements.
    gf.get_requirements(TITLE,REQUIREMENTS,('',))
    
    # This function does the rest
    f.data_analysis_1()


if __name__ == '__main__':
    main()