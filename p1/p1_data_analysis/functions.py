"""Defines six functions:

1. data_analysis_1()"""

import datetime
import pandas as pd
import pandas_datareader as pdr
import matplotlib.pyplot as plt
from matplotlib import style

def data_analysis_1():
    pd.set_option('min_rows', 60)
    start = datetime.datetime( 2010, 1, 4)
    end = datetime.datetime.now()
    df = pdr.DataReader( "XOM", "yahoo", start, end)
    

    print( "\nPrint number of records : ")
    print(len(df))

    print( "\nPrint Columns: ")
    print(df.columns)

    print( "\nPrint data frame : ")
    print(df)

    print( "\nPrint first five lines:")
    print(df[:5])

    print( "\nPrint last five lines: ")
    print(df[-5:])
    
    print( "\nPrint first 2 lines: ")
    print(df[:2])

    print( "\nPrint last 2 lines: ")
    print(df[-2:])

    #style.use('fivethirtyeight')
    style.use('ggplot')

    df['High'].plot()
    df['Adj Close'].plot()
    plt.legend()
    plt.show()